<?php
include('session.php');
if(!empty($_SESSION['login_user'])){
	$sql      = "SELECT * FROM users WHERE email = '$login_session'";
	$result   = $conn->query($sql);
	if ($result->num_rows > 0) {
		while ($row = $result->fetch_assoc()) {
			$_SESSION['user_type'] = $row["user_type"];
			$_SESSION['first_name'] = $row["first_name"];
			$_SESSION['email'] = $row["email"];
			$_SESSION['user_id'] = $row["id"];
		}
	}
}
?>

<!DOCTYPE html>
<!--[if IE 8 ]><html class="no-js oldie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="no-js oldie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html class="no-js" lang="en"> <!--<![endif]-->
<head>

   <!--- basic page needs
   ================================================== -->
   <meta charset="utf-8">
	<title>Abstract</title>
	<meta name="description" content="">  
	<meta name="author" content="">

   <!-- mobile specific metas
   ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

 	<!-- CSS
   ================================================== -->
   <link rel="stylesheet" href="css/base.css">
   <link rel="stylesheet" href="css/vendor.css">  
   <link rel="stylesheet" href="css/main.css">
        

   <!-- script
   ================================================== -->
	<script src="js/modernizr.js"></script>
	<script src="js/pace.min.js"></script>

   <!-- favicons
	================================================== -->
	<link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
	<link rel="icon" href="favicon.ico" type="image/x-icon">

</head>

<body id="top">

	<!-- header 
   ================================================== -->
   <header class="short-header">   

   	<div class="gradient-block"></div>	

   	<div class="row header-content">

   		<div class="logo">
	         <a href="index.html">Author</a>
	      </div>

	   	<nav id="main-nav-wrap">
				<ul class="main-navigation sf-menu">
					<li class="current"><a href="index.php" title="">Home</a></li>									
					<li class="has-children">
						<a href="category.html" title="">Gender</a>
						<ul class="sub-menu">
			            <li><a href="category.html">Victomization</a></li>
			            <li><a href="category.html">Age</a></li>
			         </ul>
					</li>
					<li class="has-children">
						<a href="single-standard.html" title="">Age</a>
						<ul class="sub-menu">
			            <li><a href="single-video.html">Video Post</a></li>
			            <li><a href="single-audio.html">Audio Post</a></li>
			            <li><a href="single-gallery.html">Gallery Post</a></li>
			            <li><a href="single-standard.html">Standard Post</a></li>
			         </ul>
					</li>
					<?php
					if(empty($_SESSION['first_name'])){
						echo "<li><a href='login.php' title=''>Login</a></li>";
						echo "<li><a href='signup.php' title=''>Sign up</a></li>";
					}
					else{
						if(!empty($_SESSION['user_type']) && $_SESSION['user_type'] != "0"){
							echo "<li><a href='newrequest.php' title=''>New Request</a></li>";
							echo "<li><a href='request.php' title=''>My requests</a></li>";
						}
						else{
							echo "<li><a href='request.php' title=''>Received requests</a></li>";
						}
						echo "<li><a href='logout.php' title=''>Log out</a></li>";
					}
					?>
															
				</ul>
			</nav> <!-- end main-nav-wrap -->

			<div class="search-wrap">
				
				<form role="search" method="get" class="search-form" action="#">
					<label>
						<span class="hide-content">Search for:</span>
						<input type="search" class="search-field" placeholder="Type Your Keywords" value="" name="s" title="Search for:" autocomplete="off">
					</label>
					<input type="submit" class="search-submit" value="Search">
				</form>

				<a href="#" id="close-search" class="close-btn">Close</a>

			</div> <!-- end search wrap -->	

			<div class="triggers">
				<a class="search-trigger" href="#"><i class="fa fa-search"></i></a>
				<a class="menu-toggle" href="#"><span>Menu</span></a>
			</div> <!-- end triggers -->	
   		
   	</div>     		
   	
   </header> <!-- end header -->