<?php
	include('header.php');
?>
   <!-- masonry
   ================================================== -->
	<h1>Welcome <?php echo $_SESSION["first_name"]; ?></h1> 
	<section id="bricks">
   <div class="row masonry">

   		<!-- brick-wrapper -->
         <div class="bricks-wrapper">
		 
		 <div class="grid-sizer"></div>

         	<div class="brick entry featured-grid animate-this">
         		<div class="entry-content">
         			<div id="featured-post-slider" class="flexslider">
			   			<ul class="slides">

				   			<li>
				   				<div class="featured-post-slide">

						   			<div class="post-background" style="background-image:url('images/thumbs/featured/featured-1.jpg');"></div>

								   	<div class="overlay"></div>			   		

								   	<div class="post-content">
								   		<ul class="entry-meta">
												<li>September 06, 2016</li> 
												<li><a href="#" >Naruto Uzumaki</a></li>				
											</ul>	

								   		<h1 class="slide-title"><a href="single-standard.html" title="">Minimalism Never Goes Out of Style</a></h1> 
								   	</div> 				   					  
				   			
				   				</div>
				   			</li> <!-- /slide -->

				   			<li>
				   				<div class="featured-post-slide">

						   			<div class="post-background" style="background-image:url('images/thumbs/featured/featured-2.jpg');"></div>

								   	<div class="overlay"></div>			   		

								   	<div class="post-content">
								   		<ul class="entry-meta">
												<li>August 29, 2016</li>
												<li><a href="#">Sasuke Uchiha</a></li>					
											</ul>	

								   		<h1 class="slide-title"><a href="single-standard.html" title="">Enhancing Your Designs with Negative Space</a></h1>
						   			</div>		   				   					  
				   			
				   				</div>
				   			</li> <!-- /slide -->

				   			<li>
				   				<div class="featured-post-slide">

						   			<div class="post-background" style="background-image:url('images/thumbs/featured/featured-3.jpg');;"></div>

								   	<div class="overlay"></div>			   		

								   	<div class="post-content">
								   		<ul class="entry-meta">
												<li>August 27, 2016</li>
												<li><a href="#" class="author">Naruto Uzumaki</a></li>					
											</ul>	

								   		<h1 class="slide-title"><a href="single-standard.html" title="">Music Album Cover Designs for Inspiration</a></h1>
						   			</div>

				   				</div>
				   			</li> <!-- end slide -->

				   		</ul> <!-- end slides -->
				   	</div> <!-- end featured-post-slider -->        			
         		</div> <!-- end entry content -->         		
         	</div>
	<?php
		// Check connection
		if ($conn->connect_error) {
			 die("Connection failed: " . $conn->connect_error);
		} 
		if($_SESSION["user_type"] == 1){
		$sql = "SELECT u.first_name r.* FROM request r inner join users u on u.email='$login_session' and u.id=r.user_id";
		}
		else{
			$sql = "SELECT * FROM request r inner join users u on u.id=r.user_id";
		}
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			 // output data of each row
			 while($row = $result->fetch_assoc()) {?>
				 <article class="brick entry format-standard animate-this">

               <div class="entry-text">
               	<div class="entry-header">

               		<div class="entry-meta">
               			<span class="cat-links">
               				<a href="#"><?php echo $row["date_submitted"]?></a> 
               				<a href="#"><?php echo $row["date_responded"] ?></a>               				
               			</span>			
               		</div>

               		<h1 class="entry-title"><a href="single-standard.html"><?php echo $row["name"] ?></a></h1>
               		
               	</div>
						<div class="entry-excerpt">
							<?php echo $row["description"] ?>
						</div>
						<div class="entry-excerpt">
							<?php echo "<a href='req.php?id=" . $row["request_id"] . "'>Continue</a>"; ?>
						</div>
               </div>

        		</article> <!-- end article -->
							
				<?php
			 }
		} else {
			 echo "No request";
		}

		$conn->close();
	?>
	</div>
	</div>
	

   </section> <!-- end bricks -->
<?php
	include('footer.php');
?>