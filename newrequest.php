<?php 
	include("header.php");
?>

   <!-- masonry
   ================================================== -->
   <section id="bricks">
   <div class="row masonry">
		<div style="width:60%">
			<h3>New request</h3>
				<form action="upload.php" id="newRequest" method="post" enctype="multipart/form-data">
					<input type="text" name="title" id="title" placeholder="Subject"/>
					<input type="hidden" name="MAX_FILE_SIZE" value="512000000" />
					<textarea id="description" name="description" form="newRequest"  rows="4" cols="90">Please text here...</textarea>
					<input type="file" name="fileToUpload" id="fileToUpload">
				<input type="submit" value="Send request" name="submit">
</form>
		</div>
	</div>

   </section> <!-- end bricks -->

<?php 
	include("footer.php");
?>