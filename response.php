<?php
	include('header.php');
?>

   <!-- masonry
   ================================================== -->
   <section id="bricks">
   <div class="row masonry">
   <?php
	$target_dir = "uploads/";
	$target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
	$uploadOk = 1;
	$imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
	// Check if image file is a actual image or fake image
	if(isset($_POST["submit"])) {
			$request_id = $_POST["reqid"];
			$feedback = $_POST["feedback"];
			$video_response = $_FILES["fileToUpload"]["name"];
			$fpath = "" . $_FILES["fileToUpload"]["name"];
			$res = move_uploaded_file($_FILES['fileToUpload']['tmp_name'], $target_file);
	}

	// Check connection
	if ($conn->connect_error) {
		die("Connection failed: " . $conn->connect_error);
	} 

	$sql = "INSERT INTO response (`response_id`, `request_id`, `feedback`, `video_response`)
	VALUES (NULL, '" . $request_id . "', '" . $feedback . "', '" . $video_response . "')";

	if ($conn->query($sql) === TRUE) {
		echo "Thank you for providing response.";
	} else {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
	
	//update request
	$sql = "update request set date_responded=Now() where request_id='$request_id'";
	if ($conn->query($sql) === FALSE) {
		echo "Error: " . $sql . "<br>" . $conn->error;
	}
		
	$conn->close();
?>
	
<?php
	include('footer.php');
?>