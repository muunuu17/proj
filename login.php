<?php
include("header.php");

if(!empty($_SESSION["useremail"])){
	header('Refresh: 2; URL = welcome.php');
}

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    // username and password sent from form 
    $myusername = mysqli_real_escape_string($conn, $_POST['username']);
    $mypassword = mysqli_real_escape_string($conn, $_POST['password']);
    
    $sql    = "SELECT id FROM users WHERE email = '$myusername' and password = '$mypassword'";
    $result = mysqli_query($conn, $sql);
    $row    = mysqli_fetch_array($result, MYSQLI_ASSOC);
    
    $count = mysqli_num_rows($result);
    
    // If result matched $myusername and $mypassword, table row must be 1 row
    
    if ($count == 1) {
        $_SESSION['login_user'] = $myusername;
        
        header("location: welcome.php");
    } else {
        $error = "Your Login Name or Password is invalid";
    }
}
?>
<section id="bricks">
    <div class="row masonry">
        <div style="width:60%">
            <form action = "" method = "post">
                <fieldset>
                    <div class="form-field">
                        <input name="username" type="text" id="username" class="full-width" placeholder="Email" value="">
                    </div>
                    <div class="form-field">
                        <input name="password" type="password" id="password" class="full-width" placeholder="Password" value="">
                    </div>

                    <button type="submit" class="submit button-primary">Login</button>

                </fieldset>
            </form>
            <!-- Form End -->
        </div>
    </div>

</section>
<?php
include('footer.php');
?>