<?php
	include('header.php');
?>
   <!-- masonry
   ================================================== -->
   <section id="bricks">

	<?php
		$reqid = $_GET["id"];
		$sql = "SELECT r.* FROM request r inner join users u on u.id=r.user_id where r.request_id='" . $reqid . "'";
		$result = $conn->query($sql);
		if ($result->num_rows > 0) {
			 // output data of each row
			 while($row = $result->fetch_assoc()) {?>
				 <div class="row">
				<div class="col-twelve">

					<article class="format-standard">  

						<div class="content-media">
								<div class="post-thumb">
									
								</div>  
							</div>

							<div class="primary-content">
				 <?php
				echo "<h1 class='page-title'>" . $row["name"] . "</h1>";
				echo "<ul class='entry-meta'>";
				echo "<li class='date'>" . $row["date_submitted"] . "</li>";							
				echo "</ul>";						
				echo "<p class='lead'>" . $row["description"] . "</p>";
				echo "<a href='uploads/" . $row["document"] . "'>" . "File</a>";
				
				if($row["date_responded"] == "0000-00-00" & $_SESSION["user_type"] == 0){
					echo "<a href='respond.php?id=" . $reqid . "'><h4 class='page-title'>Give response</h4></a>";
				}
				else {
					if($row["date_responded"] == "0000-00-00"){
						echo "<h4 class='page-title'>Not responded</h4>";
					}
					else{
						echo "<h4 class='page-title'>Responded</h4>";
					}
				}
				 ?>
				 </div>
				</div>
				</div>
				<?php
			 }
		} else {
			 echo "No request";
		}
		$conn->close();
	?>

   </section> <!-- end bricks -->

<?php
	include('footer.php');
?>