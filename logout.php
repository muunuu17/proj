<?php
	include("header.php");
	unset($_SESSION["username"]);
	unset($_SESSION["password"]);
	unset($_SESSION["useremail"]);
	unset($_SESSION["usertype"]);
	session_destroy();
	header('Refresh: 2; URL = login.php');
	include("footer.php");
?>