<?php include( 'header.php'); ?>
<!-- masonry
   ================================================== -->
<section id="bricks">
    <div class="row masonry">
        <div style="width:60%">
            <h3>New user</h3>
            <form name="newRequest" id="newRequest" method="post" action="newuser.php">
                <fieldset>
                    <div class="form-field">
                        <input name="email" type="text" id="email" class="full-width" placeholder="Email" value="">
                    </div>
                    <div class="form-field">
                        <input name="first_name" type="text" id="first_name" class="full-width" placeholder="First name" value="">
                    </div>

                    <div class="form-field">
                        <input name="last_name" type="text" id="last_name" class="full-width" placeholder="Last name" value="">
                    </div>

                    <div class="form-field">
                        <input name="password" type="password" id="password" class="full-width" placeholder="Password" value="">
                    </div>

                    <div class="form-field">
                        <input name="re-password" type="password" id="re-password" class="full-width" placeholder="Please type again" value="">
                    </div>

                    <button type="submit" class="submit button-primary">Sign up</button>

                </fieldset>
            </form>
            <!-- Form End -->
        </div>
    </div>

</section>
<!-- end bricks -->

<?php include( 'footer.php'); ?>