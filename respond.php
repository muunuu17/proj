<?php
	include('header.php');
?>

   <!-- masonry
   ================================================== -->
   <?php
	// Check connection
	if ($conn->connect_error) {
		 die("Connection failed: " . $conn->connect_error);
	} 
	$reqid = $_GET["id"];
	$sql = "SELECT r.* FROM request r inner join users u on u.id=r.user_id where r.request_id='" . $reqid . "'";
	$result = $conn->query($sql);
	$reqid = "";
	if ($result->num_rows > 0) {
		 // output data of each row
		 while($row = $result->fetch_assoc()) {
			$reqid = $row["request_id"];
		 }
	} else {
		 echo "No request";
	}
	$conn->close();
	?>
   <section id="bricks">
   <div class="row masonry">
		<div style="width:60%">
			<h3>Write a response</h3>
				<form action="response.php" id="newRequest" method="post" enctype="multipart/form-data">
					<input type="hidden" name="MAX_FILE_SIZE" value="512000000" />
					<textarea id="feedback" name="feedback" form="newRequest"  rows="4" cols="90">Please text here...</textarea>
					<input type="file" name="fileToUpload" id="fileToUpload">
					<?php
						echo "<input type='hidden' name='reqid' value='" . $reqid . "'/>"
					?>
				<input type="submit" value="Send response" name="submit">
</form>
		</div>
	</div>

   </section> <!-- end bricks -->

<?php
	include('footer.php');
?>